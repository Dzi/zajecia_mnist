from keras.callbacks import Callback
import matplotlib.pyplot as plt


class PlottingCallback(Callback):
    def __init__(self, keys=['loss', 'val_loss'], clear=True):
        self.epoch = []
        self.logsDict={}
        self.keys = keys
        self.clear = clear
        for k in keys:
            self.logsDict[k] = []
        
    def on_epoch_end(self,epoch, logs={}):
        self.epoch.append(epoch)
        for k in self.keys:
            if k in logs:
                self.logsDict[k].append(logs[k])
            else:
                self.logsDict[k].append(None)

    def plot(self):
        plt.figure()
        for k in self.keys:
            plt.plot(self.epoch, self.logsDict[k])
        plt.legend(self.keys)
        plt.show()
