import numpy as np


def zscore(x_train, x_test):
    x_whole = np.cocnatenate([x_train, x_test])
    mean, std = x_whole.mean(), x_whole.std()
    x_whole = (x_whole - mean) / std
    train_sz = x_train.shape[0]
    return x_whole[:train_sz], x_whole[train_sz:]


def max_min(x_train, x_test):
    maximum = max(x_train.max(), x_test.max())
    minimum = min(x_train.min(), x_test.min())
    denom = maximum - minimum
    x_train /= denom
    x_test /= denom
    return x_train, x_test
