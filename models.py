from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras import backend as K


INPUT_SHAPE = 784

if K.image_data_format() == 'channels_first':
    INPUT_SHAPE_2D = (1, 28, 28) 
else:
    INPUT_SHAPE_2D = (28, 28, 1)

OUTPUT_SHAPE = 10


def make_mlp(hidden_layer_sizes, optimizer, dropout_drop_rate):
    first_num_neurons = hidden_layer_sizes[0]

    mlp = Sequential()
    mlp.add(Dense(first_num_neurons, activation='relu', input_shape=(INPUT_SHAPE,)))
    mlp.add(Dropout(dropout_drop_rate))

    for num_neurons in hidden_layer_sizes[1:]:
        mlp.add(Dense(num_neurons, activation='relu'))
        mlp.add(Dropout(dropout_drop_rate))

    mlp.add(Dense(OUTPUT_SHAPE, activation='softmax'))

    mlp.compile(loss='categorical_crossentropy',
                  optimizer=optimizer,
                  metrics=['accuracy'])
    return mlp


def make_cnn(conv_layer_sizes, optimizer, dropout_drop_rate, kernel_size=(3, 3), dense_size=128):
    first_num_filters = conv_layer_sizes[0]
    cnn = Sequential()

    cnn.add(Conv2D(first_num_filters, kernel_size,  activation="relu", input_shape=INPUT_SHAPE_2D, padding='same'))
    cnn.add(MaxPooling2D((2, 2)))
    cnn.add(Dropout(dropout_drop_rate/2))

    for num_filters in conv_layer_sizes[1:]:
        cnn.add(Conv2D(num_filters, kernel_size, activation="relu", padding='same'))
        cnn.add(MaxPooling2D((2,2)))
        cnn.add(Dropout(dropout_drop_rate/2))

    cnn.add(Flatten())
    cnn.add(Dense(dense_size, activation="relu"))
    cnn.add(Dropout(dropout_drop_rate))
    cnn.add(Dense(OUTPUT_SHAPE, activation="softmax"))

    cnn.compile(loss="categorical_crossentropy",
                optimizer=optimizer,
                metrics=["accuracy"])
    return cnn
