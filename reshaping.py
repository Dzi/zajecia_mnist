from keras import backend as K


IMG_SHAPE = 28


def reshape_for_cnn(x_train, x_test):
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], 1, IMG_SHAPE, IMG_SHAPE)
        x_test = x_test.reshape(x_test.shape[0], 1, IMG_SHAPE, IMG_SHAPE)
    else:
        x_train = x_train.reshape(x_train.shape[0], IMG_SHAPE, IMG_SHAPE, 1)
        x_test = x_test.reshape(x_test.shape[0], IMG_SHAPE, IMG_SHAPE, 1)

    return x_train, x_test

